<?xml version="1.0" encoding="utf-8"?>
<resources>

    <string name="app_name">GitLab</string>

    <!-- Drawer -->
    <string name="nav_projects">Projects</string>
    <string name="nav_groups">Groups</string>
    <!-- Menu -->
    <string name="action_lock_orientation">Lock orientation</string>
    <string name="action_logout">Logout</string>
    <string name="action_save">Save</string>
    <string name="action_open">Open</string>
    <string name="action_open_in_browser">Open in browser</string>
    <string name="action_share">Share</string>
    <string name="action_about">About</string>
    <string name="action_search">Search</string>
    <string name="action_copy_link">Copy link</string>

    <!-- Tabs -->
    <string name="title_overview">Overview</string>
    <string name="title_commits">Commits</string>
    <string name="title_issues">Issues</string>
    <string name="title_files">Files</string>
    <string name="title_merge_requests">Merge Requests</string>
    <string name="title_members">Members</string>

    <string-array name="main_tabs">
        <item>@string/title_overview</item>
        <item>@string/title_commits</item>
        <item>@string/title_issues</item>
        <item>@string/title_files</item>
        <item>@string/title_merge_requests</item>
        <item>@string/title_members</item>
    </string-array>

    <string name="title_all">All</string>
    <string name="title_mine">Mine</string>
    <string-array name="projects_tabs">
        <item>@string/title_all</item>
        <item>@string/title_mine</item>
    </string-array>

    <string name="title_projects">Projects</string>
    <string name="title_users">Users</string>
    <string-array name="search_tabs">
        <item>@string/title_projects</item>
        <item>@string/title_users</item>
    </string-array>

    <!-- Login -->
    <string name="login_button">Login</string>
    <string name="url_hint">URL (e.g. https://gitlab.com)</string>
    <string name="url_gitlab">https://gitlab.com</string>
    <string name="user_hint">Username or Email</string>
    <string name="password_hint">Password</string>
    <string name="token_hint">Private token</string>
    <string name="login_activity">Login</string>
    <string name="login_progress_dialog">Logging in. Please wait</string>
    <string name="token_link"><u>Login using private token</u></string>
    <string name="normal_link"><u>Login using username or email</u></string>
    <string name="certificate_title">Certificate not trusted</string>
    <string name="certificate_message">The certificate the server used was not issued by a trusted authority.\n\nThis could mean a hacker is trying to intercept your communications.\n\nTo use a self-signed certificate, please follow this <a href="https://support.google.com/nexus/answer/2844832?hl=en">guide</a>.</string>

    <!-- Error messages -->
    <string name="connection_error">Connection failed</string>
    <string name="connection_error_commits">Failed to load commits</string>
    <string name="connection_error_issues">Failed to load issues</string>
    <string name="connection_error_files">Failed to load files</string>
    <string name="connection_error_users">Failed to load users</string>

    <string name="login_error">Login failed Please double-check your login information.</string>
    <string name="login_input_error">Please enter a valid url.</string>

    <string name="file_load_error">Failed to load file</string>
    <string name="save_error">Could not write to storage medium</string>
    <string name="open_error">File could not be opened</string>

    <string name="input_error">Please enter a title</string>
    <string name="user_error">User could not be added</string>
    <string name="user_remove_error">User could not be removed</string>

    <string name="files_not_supported">To browse files GitLab 5.3 or higher is needed</string>
    <string name="groups_not_supported">To add and remove users from groups GitLab 6.1 or higher is needed</string>
    <string name="not_in_group">User management is only available for projects that are assigned to groups</string>
    <string name="error_could_not_share">Could not share the link</string>
    <string name="error_no_browser">No browser on device. What are you doing?</string>
    <string name="error_user_conflict">There is a conflict in adding this user.</string>
    <string name="error_creating_account">Error creating account</string>

    <string name="required_field">Required Field</string>
    <!-- General -->
    <string name="projects">Projects</string>
    <string name="no_projects">No projects</string>
    <string name="selected_content_description">Selected</string>
    <string name="progress_dialog">Please wait</string>
    <string name="cancel_button">Cancel</string>
    <string name="save_button">Save</string>
    <string name="add_button">Add</string>
    <string name="remove_button">Remove</string>
    <string name="ok_button">OK</string>
    <string name="filter_projects">Filter projects</string>
    <string name="logout_title">Are you sure you want to log out?</string>
    <string name="failed_to_load">Failed to load</string>

    <!-- Overview -->
    <string name="no_readme_found">No README found for project</string>
    <!-- Commits -->
    <string name="no_commits_found">No commits found.</string>

    <!-- Diffs -->
    <string name="text_wrap_checkbox">Wrap text</string>

    <!-- Files -->
    <string name="file_saved">File saved successfully to Download folder</string>
    <string name="no_files_found">No files found.</string>
    <string name="copied_to_clipboard">Copied to clipboard</string>

    <!-- Issues -->
    <string name="new_note_hint">Add comment</string>
    <string name="add_issue">Add new issue</string>
    <string name="add_issue_dialog_title">New issue</string>
    <string name="title_hint">Title</string>
    <string name="description_hint">Description</string>
    <string name="add_note_content_description">Add note</string>
    <string name="issue_number">Issue #</string>

    <!-- User -->
    <string name="remove_user_dialog_title">Remove user?</string>
    <string name="add_user">Add user to group</string>
    <string name="add_user_dialog_title">Add user</string>
    <string name="no_users_found">No users found.</string>
    <string name="search_for_user">Search for user</string>
    <string name="set_project_access">Project Access</string>
    <string name="user_added_successfully">User added successfully</string>
    <string name="no_activity">No Activity</string>

    <string-array name="role_names">
        <item>Guest</item>
        <item>Reporter</item>
        <item>Developer</item>
        <item>Master</item>
        <item>Owner</item>
    </string-array>

    <!-- About -->
    <string name="about">About</string>
    <string name="contributors">Contributors</string>
    <string name="sauce">See the Sauce</string>
    <string name="source_url">https://gitlab.com/Commit451/GitLabAndroid</string>
    <string name="failed_to_load_contributors">Failed to load contributors :(</string>

    <!-- Account -->
    <string name="add_account">Add account</string>

    <!-- Merge Requests -->
    <string name="no_merge_requests">No merge requests</string>
</resources>